package homework7

/*
Задача 1. Создание и выброс собственных исключений
У Оли и Коли свой интернет-магазин.
Для своих клиентов они хотят создать личный кабинет с уникальной программой лояльности.
Если есть личный кабинет, значит, должна быть и страничка с его созданием.

Помогите Оле и Коле: реализуйте функцию, которая будет проверять логин и пароль
по определенным критериям и создавать новых пользователей.

Что нужно сделать:
1) Написать функцию, которая принимает 3 аргумента: логин, пароль, подтверждение пароля (пароль еще раз).
2) В функции учесть следующие условия:
- в логине не должно быть более 20 символов
- в пароле не должно быть менее 10 символов
- пароль и подтверждение пароля должны совпадать
3) Создать 2 класса с исключениями WrongLoginException и WrongPasswordException,
которые расширяют класс Exception(message).
4) Если не выполняются условия из п.2 - выбрасывать соответствующее исключение.
5) Функция возвращает объект класса-пользователь.
В итоге у вас получится приложение, которое будет содержать функцию для регистрации нового пользователя,
проверяя его по определенным правилам.
 */

fun main() {
    val user = checkCredentials("Дарья", "1234567890", "1234567890")
    println(user.login)
    println(user.password)
}

fun checkCredentials(login: String, password: String, passwordCfm: String): User {
    if (login.length > 20) throw WrongLoginException("В логине не должно быть более 20 символов")
    if (password.length < 10) throw WrongPasswordException("В пароле не должно быть менее 10 символов")
    if (password != passwordCfm) throw WrongPasswordException("Пароли не совпадают")
    return User(login, password)
}

class WrongLoginException(message: String): Exception(message) {

}

class WrongPasswordException(message: String): Exception(message) {

}

class User(val login: String, val password: String) {

}
