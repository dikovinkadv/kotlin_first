package homework7

/*
Задача 2. Обработка исключений

Практикант Владимир создал несколько разных классов-исключений,
объединенных в иерархию с классом Exception. Владимир совсем запутался и не понимает,
как их теперь обрабатывать. Он обращается к вам за помощью,
так как вы проходили на последнем занятии правила обработки исключений,
которые объединены в одной иерархии наследования.

Что нужно сделать:

Реализовать блок try с несколькими блоками catch в правильном порядке обработки исключений.
В блоке try можно вызывать метод exceptionTest(n:Int)
Обязательно должны быть обработаны все созданные исключения + Exception.
В итоге получится приложение, которое будет корректно обрабатывать исключения,
находящиеся в одной иерархии наследования.
 */

fun main() {
    try {
        exceptionTest(readLine()!!.toInt())
    } catch (e: CustomException2) {
        println("Поймано исключение CustomException2")
    } catch (e: CustomException5) {
        println("Поймано исключение CustomException5")
    } catch (e: CustomException1) {
        println("Поймано исключение CustomException1")
    } catch (e: CustomException3) {
        println("Поймано исключение CustomException3")
    } catch (e: CustomException4) { //write your code here
        println("Поймано исключение CustomException4")
    } catch (e: Exception) { //write your code here
        println("Поймано исключение Exception")
    }
}

open class CustomException1(message: String) : Exception(message)

open class CustomException2(message: String) : CustomException1(message)

open class CustomException3(message: String) : Exception(message)

open class CustomException4(message: String) : Exception(message)

open class CustomException5(message: String) : CustomException4(message)

fun exceptionTest(n: Int) {
    when(n) {
        1 -> throw CustomException1("CustomException1")
        2 -> throw CustomException2("CustomException2")
        3 -> throw CustomException3("CustomException3")
        4 -> throw CustomException4("CustomException4")
        5 -> throw CustomException5("CustomException5")
        else -> throw Exception("Exception")
    }
}
