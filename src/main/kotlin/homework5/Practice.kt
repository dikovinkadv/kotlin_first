package homework5
/*
4.2 Функции. Задачи для самостоятельной практики
 */

/*
Задача 1: FooBar
Нужно написать функцию fooBar, которая принимает целое числo и возвращает строку. Если:
- число делится на 3 без остатка, то вернуть строку "Foo"
- число делится на 5 без остатка, то вернуть строку "Bar"
- число делится и на 3 и на 5 без остатка, то вернуть строку "FooBar"
- иначе вернуть строку "???"
 */

fun main() {
    println("Ответ fooBar(6) = ${fooBar(6)}")
    println("Ответ fooBar(10) = ${fooBar(10)}")
    println("Ответ fooBar(15) = ${fooBar(15)}")
    println("Ответ fooBar(4) = ${fooBar(4)}")
}

//с конструкцией if-else
fun fooBar(param: Int): String {
    var retString = "???"
    if (param % 3 == 0 && param % 5 == 0) {
        retString = "FooBar"
    } else if (param % 3 == 0) {
        retString = "Foo"
    } else if (param % 5 == 0)
        retString = "Bar"
    return retString
}

//с конструкцией when
fun fooBar(param: Int): String {
    val retString = when {
        param % 3 == 0 && param % 5 == 0 -> "FooBar"
        param % 3 == 0 -> "Foo"
        param % 5 == 0 -> "Bar"
        else -> "???"
    }
    return retString
}

/*
Задача 2: Факториал
Нужно написать функцию factorial, которая принимает целое число n типа Int и возвращает целое число равное факториалу входного числа (n!).
Для задачи представим, что во входной параметр будут передаваться только числа от 1 до 20 включительно.
Подбери целочисленный тип данных, который вместит факториал 20! (подумай, какой тип можно использовать чтобы вместить факториал большего числа?).
Для решения задачи используй цикл.
 */

fun main() {
    assert(factorial(1) == 1L) { "Неверный ответ 1!"}
    assert(factorial(5) == 120L) { "Неверный ответ 5!"}
    assert(factorial(20) == 243290200817664000L) { "Неверный ответ 20!"}
    println("Всё верно")
}

fun factorial(param: Int): Long {
    var result = 1
    for (i in 1..param) {
        result *= i
    }
    return result.toLong()
}

/*
Задача 3: Факториал. Рекурсия
 */

fun main() {
    assert(factorialRecursive(1) == 1L) { "Неверный ответ 1!"}
    assert(factorialRecursive(5) == 120L) { "Неверный ответ 5!"}
    assert(factorialRecursive(20) == 243290200817664000L) { "Неверный ответ 20!"}
    println("Всё верно")
}

fun factorialRecursive(n: Int): Long {
    return if (n <= 1) 1L else n.toLong() * factorialRecursive(n - 1)
}

/*
4.2 Функции. Задачи для самостоятельной практики
 */

/*
Задача Button
Нужно создать класс "Кнопка" (Button).

У кнопки есть свойства:
- Неизменяемое свойство title (строка — что написано на кнопке),
- Изменяемое свойство isLoading (отображается ли на кнопке сейчас лоадер) — по умолчанию лоадер не отображается
- Изменяемое свойство clickCount (количество нажатий на кнопку) — начальное значение 0
При создании кнопки в конструкторе:
- Обязательно нужно указать title
- Можно указать isLoading
- Нельзя задать clickCount
У кнопки есть метод click(), при вызове метода:
- Если на кнопке сейчас отображается лоадер, нужно вывести сообщение "Нельзя нажимать во время загрузки"
- Иначе вывести сообщение "Нажали на кнопку" и увеличить clickCount на 1
 */

/*
Дополнительное задание
Нужно сделать так, чтобы clickCount можно было прочитать снаружи, но нельзя изменить.
Для этого может пригодиться информация о модификаторах доступа и свойствах из документации.
 */

class Button(val title: String, var isLoading: Boolean = false) {
    var  clickCount = 0
    fun click() {
        if (isLoading) {
            println("Нельзя нажимать во время загрузки")
        } else {
            clickCount++
            println("Нажали на кнопку")
        }
    }
}

fun main() {
    val button = Button(title = "Кнопочка")
    button.click()
    println("Нажатий на кнопку = ${button.clickCount}")
    button.isLoading = true
    button.click()
    println("Нажатий на кнопку = ${button.clickCount}")
}
