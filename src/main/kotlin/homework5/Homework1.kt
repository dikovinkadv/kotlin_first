package homework5

/*
Задача 1. Создание классов
Алексей и Никита — братья, они с раннего детства мечтали открыть собственный зоопарк.
Посовещавшись друг с другом, они определились с животными, которые будут в их зоопарке:
- лев
- тигр
- бегемот
- волк
- жираф
- слон
- шимпанзе
- горилла
Ребятам предстоит поддерживать учет всех животных, которых они запланировали завести. Помогите им создать классы для учета.
Что нужно сделать:
1. Создать указанные классы животных.
2. Добавить для каждого класса набор полей:
имя
рост
вес
предпочтения в еде (массив строк)
сытость
3. В конструкторе по умолчанию должны указываться только имя, рост и вес.
4. Добавить к классам метод “поесть”, в который передается текстовое значение еды.
5. Если еда совпадает с предпочтениям, то сытость животного увеличивается.
 */

fun main() {
    val leva = Lion("Лева", 1.8, 150.0)
    val tigrik = Tiger("Тигрик", 1.9, 160.0)
    val gipopotamys = Hippo("Гипопотамус", 3.0, 400.0)
    val volchenok = Wolf("Волченок", 0.7, 60.0)
    val girafina = Giraffe("Жирафина", 4.2, 1000.0)
    val slonila = Elephant("Слонила", 7.5, 3000.0)
    val shipanzulya = Chimpanzee("Шимпанзуля", 1.7, 60.0)
    val gorillochka = Gorilla("Гориллос", 1.7, 190.0)

    tigrik.eat("Оленя")
    gipopotamys.eat("Траву")
    volchenok.eat("Зайца")
    girafina.eat("Фрукт")
    slonila.eat("Банан")
    shipanzulya.eat("Птицу")
    gorillochka.eat("Побеги")

    // Убедиться, что метод eat() работает корректно
    leva.eat("Антилопу") // элемент есть в массиве satiety +1
    leva.eat("Косулю") // элемент есть в массиве satiety +1
    leva.eat("Насекомое") // элемента нет в массиве
    leva.eat("Чупокабру") // элемента нет в массиве
    println("Сытость Левы = " + leva.satiety) // 2
}
abstract class Animal(val name: String, var height: Double, var weight: Double) {
    abstract val foodPreferences: Array<String>
    var satiety: Int = 0

    //Добавила метод eat в абстрактный класс, чтобы уменьшить дубляж кода
    fun eat(food: String) {
        if (foodPreferences.contains(food)) {      // contains() возвращает true, если в массиве есть переданный элемент
            satiety++
            println("$name съел '$food'")
        } else {
            println("$name не ест '$food'")
        }
    }
}

class Lion(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences: Array<String> = arrayOf("Антилопу", "Косулю")
}
class Tiger(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Оленя", "Рыбку", "Птичку")
}
class Hippo(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Траву", "Фрукт")
}
class Wolf(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Зайца", "Лосяша", "Кабана")
}
class Giraffe(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Листья", "Фрукт")
}
class Elephant(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Банан", "Яблоко", "Печенье", "Хлеб")
}
class Chimpanzee(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Фрукт", "Листья", "Насекомое", "Птицу")
}
class Gorilla(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Листья", "Побеги", "Фрукт")
}
