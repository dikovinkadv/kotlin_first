package homework5
/*
Задача 2. Реализация функции для работы с числами
Девочка Юля увлекается цифрами и очень любит проводить разные эксперименты с ними.
На уроке информатике преподаватель, зная об интересах Юли, задал ей персональное домашнее задание:
«Написать функцию, которая принимает в качестве аргумента целое число и
возвращает перевернутое значение введенного числа, например 2145 = 5412».
Юля хорошо разбирается в числах, а вот программирование дается ей нелегко.
Давайте ей поможем решить эту задачу!

Рекомендации:
Преподаватель не оставил Юлю «тонуть в бассейне языка Kotlin» и дал небольшую подсказку:
остаток от деления — твой спасательный круг!
Не совсем понятно, что он имел ввиду, но может это как-то пригодится. =)

Что нужно сделать
Написать функцию, которая:
- Принимает в качестве аргумента целое число Int.
- Возвращает “перевернутое” число типа Int.

Как проверить:
- Запустить приложение.
- Ввести любое целое число больше одного знака, например 2145.
- На экран выводится перевернутое число, например 5412.
В итоге получится функция, которая будет “переворачивать” полученное число.
 */

fun main() {
    val digit = readLine()!!.toInt()
    val reversed = flipDigit(digit)
    println("Reversed Number: $reversed")
}

fun flipDigit(number: Int): Int {
    var reversed = 0
    var numb = number

    while (numb != 0) {
        val digit = numb % 10
        numb /= 10
        reversed = reversed * 10 + digit
    }
    return reversed
}
