package homework4
/*
Задача 1. Перебор элементов массива и вывод сообщения на экран в зависимости от значения
*/
fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var evenJoe = 0
    var oddTeam = 0

    for (i in myArray) {
        if (i % 2 == 0) {
            evenJoe++
        } else {
            oddTeam++
        }
    }

    println("Количество четных монет, полученных Джо: $evenJoe")
    println("Количество нечетных монет, полученных командой: $oddTeam")
}
