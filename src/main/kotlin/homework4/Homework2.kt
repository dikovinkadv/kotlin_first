package homework4
/*
Задача 2. Поиск максимального элемента массива
*/
fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var loser = 0.0
    var threes = 0.0
    var good = 0.0
    var excellent = 0.0

    for (i in marks) {
        when (i) {
            2 -> loser++
            3 -> threes++
            4 -> good++
            5 -> excellent++
        }
    }

    val excellentPercent = excellent / marks.size * 100
    val goodPercent = good / marks.size * 100
    val threesPercent = threes / marks.size * 100
    val loserPercent = loser / marks.size * 100

    println("Отличников - $excellentPercent%")
    println("Хорошистов - $goodPercent%")
    println("Троечников - $threesPercent%")
    println("Двоечников - $loserPercent%")
}
