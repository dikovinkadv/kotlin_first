package homework2

fun main() {
    val lemonade: Int = 18500
    val pinaColada: Short = 200
    val whiskey: Byte = 50
    val fresh: Long = 3000000000
    val cola: Float = 0.5F
    val ale: Double = 0.666666667
    val specialtyDrink: String = "Что-то авторское!"

    println("Илья разлил $lemonade мл лимонада")
    println("Илья разлил $pinaColada мл пина колады")
    println("Илья разлил $whiskey мл виски")
    println("Илья разлил $fresh капель фреша")
    println("Илья разлил $cola литра колы")
    println("Илья разлил $ale литра эля")
    println("Илья разлил $specialtyDrink")
}
