package homework8

/*
Задача 2. Работа с ассоциативными массивами Map

Пенсионер Ольга Алексеевна пошла в магазин за продуктами.
В магазине для пенсионеров действует скидка в 20% на определенные товары.
Помогите ей правильно посчитать количество овощей в корзине и стоимость товаров с учетом скидки на некоторые из них.

Что нужно сделать:
Реализовать метод для подсчета количества овощей в корзине. Определить овощ в корзине поможет vegetableSet.
Реализовать метод для расчета стоимости всех товаров в корзине, с учетом скидок на определенные товары. Цены товаров описаны в массиве prices, размер скидки в переменной discountValue, а список товаров по скидке во множестве discountSet.
В итоге получится приложение, которое содержит функции для подсчета овощей в корзине и для расчета стоимости товаров.

Как проверить:
Запустить приложение.
Количество овощей должно быть 7.
Сумма товаров должна быть равна 821.1199999999999.
 */

val userCart = mutableMapOf<String, Int>(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println("Количество овощей должно быть ${calcVegetables(userCart)}")
    println("Сумма товаров должна быть равна ${amountPurchases(userCart)}")
}

fun calcVegetables(basket: MutableMap<String, Int>): Int {
    var sumVegetables = 0
    vegetableSet.forEach {
        if (basket.containsKey(it)) {
            sumVegetables += basket.getValue(it)
        }
    }
    return sumVegetables
}

fun amountPurchases(basket: MutableMap<String, Int>): Double {
    var amount = 0.0
    val discount = 1 - discountValue
    for ((key, value) in basket) {
        if ((prices.containsKey(key)) && (discountSet.contains(key))) {
            amount += (value * prices[key]!!) * discount
            continue
        }
        if (prices.containsKey(key)) {
            amount += value * prices[key]!!
        }
    }
    return amount
}
