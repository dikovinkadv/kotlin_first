package homework3

fun main() {
    var isSale: Boolean = false
    val price = if (isSale == true) 50 else 100
    println(price)

    var color: String = "yellow" // green or yellow
    val message = if (color == "red") "Стой!" else if (color == "green") "Иди" else "Погодь"
    println(message)

    when (color) {
        "yellow" -> { println("Погодь") }
        "green" -> { println("Иди") }
        "red" -> { println("Стой!") }
        else -> { println("Не знать такого цвета") }
    }

/*
Задача 1
    Нужно проверить, есть ли в массиве число 4.
    Если число есть в массиве, то вывести сообщение об этом в консоль и выйти из цикла.
    Если числа нет, то вывести соответствующее сообщение.
    Проверить работу на массивах val array1 = arrayOf(1, 2, 3, 4, 5, 6, 7) и val array2 = arrayOf(1, 2, 3, 5, 6, 7).
 */

    val array1 = arrayOf(1, 2, 3, 4, 5, 6, 7)
    val array2 = arrayOf(1, 2, 3, 5, 6, 7)
    val chislo = 4

    for (i in array1.indices) {
        if (array1[i] == chislo) {
            println("В массиве есть $chislo")
            return
        }
    }
    println("В массиве нет $chislo")

/*
 Задача 2
    Есть массив чисел val array = arrayOf(1, -2, 3, -4, 5, -6, 7, -8, 9).
    Нужно найти сумму всех положительных чисел в массиве и записать в переменную var result: Int.
    Результат вывести в консоль.
*/

    val array3 = arrayOf(1, -2, 3, -4, 5, -6, 7, -8, 9)
    var result = 0

    for (i in array3.indices) {
        if (array3[i] > 0) {
            result += array3[i]
        }
    }
    println("Сумма всех положительных чисел в массиве = $result")

/*
 Задача 3
     Дан диапазон чисел от 0 до 10. Найти суммы четных и нечетных чисел.
     Сравнить эти значения и вывести результат в консоль.
*/

    var chetnie = 0
    var nechetnie = 0

    for (i in 0 until 11) {
        if (i % 2 == 0) {
            chetnie += i
        } else
            nechetnie += i
    }
    if ( chetnie > nechetnie) {
        println("Сумма четных ($chetnie) больше суммы нечетных ($nechetnie)")
    } else if (chetnie < nechetnie) {
        println("Сумма четных ($chetnie) меньше суммы нечетных ($nechetnie)")
    } else {
        println("Сумма четных ($chetnie) равна сумме нечетных ($nechetnie)")
    }

/*
    Задача 4
    Дан диапазон чисел от 0 до 10.
    Необходимо пройтись по нему в обратном порядке и используя
    ключевое слово step вывести в консоль только нечетные значения.
*/

    for (i in 9 downTo 0 step 2) {
        println(i)
    }

/*
    Задача 5
    Дан массив чисел val array = arrayOf(1, 5, 7, 3, 4, 8, 2, 6).
    Необходимо суммировать значения массива до тех пор, пока их сумма не будет больше или равна 21.
    Необходимо использовать while. Результат вывести в консоль.
*/

    val array4 = arrayOf(1, 5, 7, 3, 4, 8, 2, 6)
    var sumArray = 0
    var i = 0
    val limit = 21

    while (i < array4.size && sumArray + array4[i] < limit) {
        sumArray += array4[i]
        i++
    }
    println("$sumArray - cумма значениий массива до тех пор, пока их сумма не будет больше или равна 21")
}
