package homework6

/*
Задача 1
Рекомендации
Определите иерархию наследования классов-животных в зоопарке Алексея и Никиты и не забудьте,
что некоторые классы не должны создавать объекты.

Что нужно сделать:
Определите суперкласс для животных.
С помощью наследования определите иерархию наследования классов-животных и вынесите в суперкласс свойства и функции,
которые считаете повторяющимися.
Пометьте свойства или функции нужным модификатором доступа.

Как проверить:
Все 8 классов-животных объединены в иерархию наследования.
Каждый класс-животное обладает всеми 5 свойствами, как раньше.
Каждый класс содержит функцию eat().
Создать объект определенного класса-животного и вызвать функцию eat().
Проверить, что метод eat() работает корректно.
В итоге получится приложение, в котором 8 классов-животных будут объединены в иерархию наследования.


Задача 2
Алексей и Никита кормят животных по отдельности, вызывая метод eat() для каждого из них.
Кормление зверей занимает огромное количество времени. Ребята просят вас реализовать функцию,
которая с помощью полиморфизма покормит всех животных в зоопарке.

Что нужно сделать:
Написать функцию, которая в качестве аргументов принимает 2 массива: животных и строк (типов еды).
Функция Unit не возвращает ничего, просто кормит всех животных.

Как проверить:
Запустить приложение
Передать в качестве аргументов массив объектов-животных и массив со строками-едой.
Для каждого животного вызывается свой метод eat().
В итоге получится функция, которая будет реализовывать логику кормления всех животных, независимо от их типа.
 */

fun main() {
    val leva = Lion("Лева", 1.8, 150.0)
    val tigrik = Tiger("Тигрик", 1.9, 160.0)
    val gipopotamys = Hippo("Гипопотамус", 3.0, 400.0)
    val volchenok = Wolf("Волченок", 0.7, 60.0)
    val girafina = Giraffe("Жирафина", 4.2, 1000.0)
    val slonila = Elephant("Слонила", 7.5, 3000.0)
    val shipanzulya = Chimpanzee("Шимпанзуля", 1.7, 60.0)
    val gorillochka = Gorilla("Гориллос", 1.7, 190.0)

    // 2 массива: животных и типов еды
    val arrayAnimals = arrayOf(leva, tigrik, gipopotamys, volchenok, girafina, slonila, shipanzulya, gorillochka)
    val arrayFood = arrayOf("Антилопу", "Траву", "Фрукт", "Банан", "Птицу", "Побеги", "Насекомое", "Косулю")

    feedAnimals(arrayAnimals, arrayFood)

    // Убедиться, что Лева покушал
    println("Сытость Левы = " + leva.satiety) // 2
}

//функция кормит всех животных
fun feedAnimals(arrayAnimals: Array<Animal>, arrayFood: Array<String>) {
    for (animal in arrayAnimals) {
        for (food in arrayFood) {
            animal.eat(food)
        }
    }
}

abstract class Animal(val name: String, var height: Double, var weight: Double) {
    abstract val foodPreferences: Array<String>
    var satiety: Int = 0

    //Добавила метод eat в абстрактный класс, чтобы уменьшить дубляж кода
    fun eat(food: String) {
        if (foodPreferences.contains(food)) {      // contains() возвращает true, если в массиве есть переданный элемент
            satiety++
            println("$name съел '$food'")
        } else {
            println("$name не ест '$food'")
        }
    }
}

class Lion(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences: Array<String> = arrayOf("Антилопу", "Косулю")
}

class Tiger(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Оленя", "Рыбку", "Птичку")
}

class Hippo(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Траву", "Фрукт")
}

class Wolf(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Зайца", "Лосяша", "Кабана")
}

class Giraffe(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Листья", "Фрукт")
}

class Elephant(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Банан", "Яблоко", "Печенье", "Хлеб")
}

class Chimpanzee(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Фрукт", "Листья", "Насекомое", "Птицу")
}

class Gorilla(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Листья", "Побеги", "Фрукт")
}
