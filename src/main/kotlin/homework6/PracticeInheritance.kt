package homework6
/*
НАСЛЕДОВАНИЕ
Задача 1
Необходимо создать следующие классы наследники TextElement, ImageElement.
Так же необходимо создать класс EditTextElement и отнаследовать его от класса TextElement.
В каждом классе наследнике нужно создать хотя бы по одной функции имитирующей проверку релевантную для конкретного класса.
Например, для класса TextElement можно создать функцию assertText(), которая будет имитировать проверку текста.
Реализовать функции проверок нужно с помощью функции println, как примеру в базовом классе.
Дополнительно можно добавить в базовые классы такую(ие) проверки, которые были бы актуальны для всех дочерних классов.
 */
fun main() {
    val textElement = TextElement()
    val editTextElement = EditTextElement()
    val imageElement = ImageElement()

    textElement.assertIsDisplayed()
    textElement.assertText()
    editTextElement.assertText() // функция assertText доступна, так как EditTextElement является наследником TextElement

  //  imageElement.assertText() // функция assertText не доступна, так как ImageElement не является наследником TextElement
    imageElement.assertIsDisplayed() // assertIsDisplayed доступна, так как она объявлена в базовом классе BaseElement

    //чек задачи 2
    val loginScreen = LoginScreen()
    val settingsScreen = SettingsScreen()

    loginScreen.testLoginScreen()
    settingsScreen.testSettingsScreen()
}
open class BaseElement() {

    fun assertIsDisplayed() {
        println("проверяем, что элемент отображается на экране")
    }
}

open class TextElement (): BaseElement() {
    fun assertText() {
        println("проверяем текст")
    }
}

class ImageElement (): BaseElement() {
    fun assertImage() {
        println("проверяем изображение")
    }
}

class EditTextElement (): TextElement() {
    fun assertEditText() {
        println("проверяем редактирование текста")
    }

    fun checkHint (string: String) {
        println(string)
    }
}

class CheckBoxElement (): BaseElement() {
    fun assertEditText() {
        println("проверяем чекбокс")
    }

    fun assertIsSelected (boolean: Boolean) {
        println(boolean)
    }
}

/*
Задача 2
Необходимо найти закономерности и вынести общую логику в базовый класс BaseScreen,
от которого потом отнаследовать LoginScreen и SettingsScreen. Базовый класс BaseScreen лучше сделать абстрактным,
а не просто open. Таким образом нельзя будет создать его экземпляр.

В результате у вас должен получиться абстрактный класс BaseScreen и два отнаследованных от него класса
LoginScreen и SettingsScreen. В классах наследниках должны быть переопределены некоторые свойства и функции,
например, свойство tag.
 */

abstract class BaseScreen() {
    abstract val tag: String
    abstract val hasStatusBar: Boolean
}

class LoginScreen(): BaseScreen() {
    override val tag: String = "Login Screen"
    override val hasStatusBar: Boolean = false

    private val loginField: EditTextElement = EditTextElement()
    private val passwordField: EditTextElement = EditTextElement()

    fun testLoginScreen() {
        loginField.checkHint("Введите логин")
        passwordField.checkHint("Введите пароль")
    }
}

class SettingsScreen(): BaseScreen() {
    override val tag: String = "Settings Screen"
    override val hasStatusBar: Boolean = true

    private val soundEnabled: CheckBoxElement = CheckBoxElement()
    private val showHints: CheckBoxElement = CheckBoxElement()

    fun testSettingsScreen() {
        soundEnabled.assertIsSelected(false)
        showHints.assertIsSelected(true)
    }
}
