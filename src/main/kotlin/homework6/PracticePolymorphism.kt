package homework6
/*
ПОЛИМОРФИЗМ
Задача 1
Дан абстрактный класс Apartment()
Необходимо создать наследников для одно, двух и трехкомнатных квартир
OneRoomApartment, TwoRoomApartment и ThreeRoomApartment.
В каждом классе переопределить функцию displayInfo() в которой нужно вывести информацию о номере квартиры и этаже,
а также дополнительную информацию, например количество санузлов, отдельно ли кухня и т.д.
При вызове этой функции должно получиться что-то вроде "Однокомнатная квартира №10 на 5 этаже.
Одна спальня и один санузел. Кухня отдельно."
После создания всех указанных классов, необходимо создать класс ApartmentManager,
внутри которого создать массив apartments: Array<Apartment> и наполнить его квартирами разного типа.
В этом классе также необходимо реализовать функцию getAllApartmentsInfo(),
в которой будет выводиться информация о всех квартирах из массива apartments.

    Вывод:
    Однокомнатная квартира №10 на 2 этаже. Одна спальня и один санузел. Кухня отдельно.
    Двухкомнатная квартира №22 на 3 этаже. Две спальни и один санузел. Кухня отдельно.
    Трехкомнатная квартира №65 на 10 этаже. Две спальни и два санузла. Кухня отдельно.
 */

abstract class Apartment (abstract val number: Int, abstract val floor: Int) {
    abstract fun displayInfo()
}

class OneRoomApartment(override val number: Int, override val floor: Int) : Apartment (number, floor) {
    override fun displayInfo() {
        println("Однокомнатная квартира №$number на $floor этаже. Одна спальня и один санузел. Кухня отдельно.")
    }
}

class TwoRoomApartment(override val number: Int, override val floor: Int) : Apartment (number, floor) {
    override fun displayInfo() {
        println("Двухкомнатная квартира №$number на $floor этаже. Две спальни и один санузел. Кухня отдельно.")
    }
}

class ThreeRoomApartment(override val number: Int, override val floor: Int) : Apartment (number, floor) {
    override fun displayInfo() {
        println("Трехкомнатная квартира №$number на $floor этаже. Две спальни и два санузла. Кухня отдельно.")
    }
}

class ApartmentManager {
    var apartments: Array<Apartment> = arrayOf()
    fun getAllApartmentsInfo(apartments: Array<Apartment>) {
        for (apartament in apartments) {
            apartament.displayInfo()
        }
    }
}

fun main() {
    val manager = ApartmentManager()
    manager.apartments = arrayOf(
        OneRoomApartment(10, 2),
        TwoRoomApartment(22, 3),
        ThreeRoomApartment(65, 10)
    )
    manager.getAllApartmentsInfo()
